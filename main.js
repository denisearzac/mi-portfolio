let nav = document.getElementById('nav');
let boton = document.getElementById("icono");
let enlaces = document.getElementById("enlaces");
let contador = 0;
let spanCollapse = document.getElementById("collapse");
let spanUnCollapse = document.getElementById("uncollapse");
let menu = document.getElementById('enlaces');

function menus() {
    let Desplazamiento_Actual = window.pageYOffset;

    if (Desplazamiento_Actual <= 330) {
        nav.classList.remove('nav2');
        nav.className = ('nav1');
        nav.style.transition = '1s'
        menu.style.top = '70px';
        //  abrir.style.color = 'white'
    } else {
        nav.classList.remove('nav1');
        nav.className = ('nav2');
        nav.style.transition = '1s';
        menu.style.top = '70px';
        //abrir.style.color = 'white'


    }
}

window.addEventListener('scroll', function() {
    console.log(window.pageYOffset);
    menus();
});


boton.addEventListener("click", function() {
    if (contador == 0) {
        enlaces.className = ('enlaces dos');
        contador = 1;
        spanCollapse.className = ('hide');
        spanUnCollapse.classList.remove('hide');


    } else {
        enlaces.classList.remove('dos');
        spanCollapse.classList.remove('hide');
        spanUnCollapse.className = ('hide');
        enlaces.className = ('enlaces uno');
        contador = 0;

    }
})



window.addEventListener('click', function(e) {
    console.log(e.target);

    if (contador == 1 && e.target.id != "collapse") {
        enlaces.classList.remove('dos');
        enlaces.className = ('enlaces uno');
        contador = 0;
        spanCollapse.classList.remove('hide');
        spanUnCollapse.className = ('hide');
    }

});



$(function() {
    let inicio = $('#inicio').offset().top,
        conocimientos = $('#conocimientos').offset().top,
        portfolio = $('#portfolio').offset().top,
        contacto = $('#contacto').offset().top;

    // window.addEventListener('resize', function() {
    //     let inicio = $('#inicio').offset().top,
    //         conocimientos = $('#conocimientos').offset().top,
    //         portfolio = $('portfolio').offset().top,
    //         contacto = $('#contacto').offset().top;
    // });



    $('#enlace-inicio').on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: 0
        }, 600);
    });


    $('#enlace-conocimientos').on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: conocimientos - 25
        }, 600);

    });
    $('#enlace-portfolio').on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: portfolio - 100
        }, 600);

    });

    $('#enlace-contacto').on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: contacto
        }, 600);

    });

});